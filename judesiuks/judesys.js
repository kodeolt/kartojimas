/*
	JavaScript judantys elementai

	Pasiruosimas: 
	html failas su div, kurio viduj bus musu judantis elementas

	1. judes pats
	1.1. tikrinti rezius
	1.2. keisti krypti

 */

// console.log('veikia');

var circle = document.querySelector('.circle');

// Pradines koordinates apskritimo
var x = 10;
var y = 10;

// Judejimo greitis
var dx = 50; 
var dy = 50; 

// Max X ir max Y
var maxX = window.innerWidth;
var maxY = window.innerHeight;

circle.style.left = x + 'px';
circle.style.top = y + 'px';


setInterval(function() {
	// X koordinaciu tikrinimas
	if(x + 50 + dx > maxX || x + dx < 0) {
		dx = -dx;
	}

	// Y koordinaciu tikrinimas
	if(y + 50 + dy > maxY || y + dy < 0) {
		dy = -dy;
	}

	// Keisti apskritimo koordinates
	x = x + dx;
	y = y + dy;

	circle.style.left = x + 'px';
	circle.style.top = y + 'px';

}, 25);